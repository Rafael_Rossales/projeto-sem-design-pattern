<h2>O que é um padrão de comportamento?</h2>
<p>Na engenharia de software, os padrões de design comportamental são padrões que definem métodos de comunicação entre objetos. Consequentemente estes padrões aumentam a flexibilidade na execução do algoritmo. Neste projeto apresentamos um dos mais importantes padrões o Template Method Pattern. Mas antes de implementar-mos o código vejamos a definição do Template Method Pattern.</p>

<h2>Template Method Pattern (Padrão de Metodo Template)</h2>
<p>É um dos padrões pertencente ao grupo de padrões de design comportamentais (Behavioural Patterns).Este padrão de design define os passos de um algoritmo e permite que a implementação de um ou mais desses passos seja executada pelas subclasses ao qual a superclasse for extendida.Para que isso seja possivel o Template Method protege o algoritmo e fornecendo métodos abstratos para que as subclasses possam ser implementados.</p>

<h2>E quanto aos <i>Hooks</i> (ganchos)</h2>
<p>É comum no template method a utilização de funções conhecidas como “Hooks”. Que são métodos que  geralmente não precisam ser  implementados na superclasse, porém estes métodos podem ser ou não implementados nas subclasses.Entretanto é importante especificar no template method quais operações são hooks e quais são operações abstratas (que precisam ser sobrescritas ). Para o reuso efetivo da classe abstrata pelas subclasses que precisam entender quais operações são designadas para serem sobrescritas.</p>

<h3>Exemplo de Métodos Hook</h3>
![hooks](/uploads/15c1049c60946e12506738c83392e683/hooks.png)

<h2>Template Method na Pratica</h2>
<p>Para entender a estrutura do template method  vamos utilizar como metáfora a construção de uma casa, para construção de uma casa existem passos fundamentais que devem ser seguidos como: construção da fundação, pilares ,paredes, aberturas e telhado. É importante ressaltar que não se deve mudar a ordem de execução das função pois não podemos pôr janelas sem paredes.Analisando o código abaixo percebemos que a fundação de uma casa, é igual para todos os tipos de casas, independente se a casa é feita de madeira ou vidro. 
Partindo deste ponto podemos então implementar a superclasse com os métodos que serão comums entre as classes. Para isso é preciso definir o template como 
<i>classe abstrata</i>, e declarar os métodos que não devem ser sobrescritos como <i>final</i></p>


<strong>CasaTemplate.java</strong>

![Sem_título](/uploads/8b766121bd593dcb62d0ab72adc0bc06/Sem_título.png)

<p>ConstruirCasa() é o template method que define a ordem de execução de cada passo do processo de execução do algoritmo.</p>

<p><strong>CasaMadeira.java</strong></p>

![casademadeira](/uploads/4b6b488b5812796033d2222f3cac4fb0/casademadeira.png)


<p>Outros métodos do template  poderiam ser sobrescrever caso necessário.</p>

<p><strong>CasaVidro.java</strong></p>

![casavidro](/uploads/253c1397154c5ff13be80bd63756cea6/casavidro.png)

<p><strong>ClassePrincipal</strong></p>

![classePrincipal](/uploads/6636825eaefe426191dfe9337b917869/classePrincipal.png)

<p><strong>Resultado</strong></p>


![result](/uploads/c3f3de847c36ee7c470bacfb21b815db/result.png)



<h4>Links para clonar repositórios</h4>
<p>[Algoritmo usando Template Pattern](https://gitlab.com/Rafael_Rossales/projeto-sem-design-pattern.git)</p>
<p>[Algoritmo sem Template Pattern](https://gitlab.com/Rafael_Rossales/projeto-usando-design-pattern.git)</p>

<h2>Template Method em outras linguagens ou API's</h2>
<p>O Template Method também pode ser implementado por outras linguagens como PHP , C# ,C++ ,Java ,Python entre outras.Pode ser utilizado em API’s ou Frameworks.
Este método esta presente em uma  API muito utilizada em java a API swing, onde a classe JFrame define o método paint() como abstrato para que seja  implementado nas subclasses que estendem JFrame.
Os Applets também utilizam o padrão Template Method através dos métodos  como init(),start(),stop(),destroy(), e outros.
</p>

<hr>

<h4>Vantagens na utilização do Template Method pattern</h4>
<ul>
<li>Elimina a duplicação de código de maneira desnecessária.</li>
<li>O padrão de projeto quando aplicado, cria uma linguagem universal para os programadores, aumentando assim a eficiência na leitura do código.
</li>
</ul>

<h4>Desvantagens do Template Method pattern</h4>
<ul>
<li>Limita o desenvolvimento do “esqueleto” do algoritmo</li>
<li>Se uma das classes precise de comportamento de duas outras classes só poderá fazer o uso da herança para uma delas.</li>
<li>Após o instanciamento da implementação  não será mais possível alterar os passos do algoritmo.</li>
<li>Se muitos métodos da classe que contém o método template forem abstratos, isso pode atrapalhar o projeto em termos de consumo de memória.</li>
</ul>

<h1>Conclusão</h1>
<p>Esse padrão é um dos poucos que  têm restrições  contra sua implementação, até porque o principal objetivo dele é remover código duplicado. Logo quando houver  a oportunidade, aplique o Template Method.
Vale lembrar também que esse padrão vai permitir a definir os passos exatos de um  algoritmo, onde alguns desses passos, mais específicos, são implementados na subclasse.
</p>


