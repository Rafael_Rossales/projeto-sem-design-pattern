/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication4;

/**
 *
 * @author Rafael_Rossales
 */
public abstract class CasaTemplate {

    public final void ConstruirCasa() { // Metodo que define a ordem de execução dos metodos
        construirFundacao();
        construirPilares();
        construirParede();
        construirJanela();
        
        System.out.println("Casas Construida!");

    }
    
    private void construirJanela(){
        System.out.println("Construindo Janela");
    }
    
    // Metodos a serem implementados
    public abstract void construirParede(); 
    public abstract void construirPilares();
    
    private void construirFundacao(){
        System.out.println("Construindo Fundacao com cimento,ferro e areia...");
    }
    
    
    
        
    
        
    

}
